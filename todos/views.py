from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todo = TodoList.objects.all()
    context = {
        "todo_list_list": todo,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo = form.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo.id)
    else:
        form = TodoListForm(instance=todo)

    context = {
        "todo_object": todo,
        "todo_form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save(False)
            item.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/items_create.html", context)


def todo_item_update(request, id):
    todo_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)
    context = {
        "todo_item": todo_item,
        "form": form,
    }
    return render(request, "todos/items_edit.html", context)
