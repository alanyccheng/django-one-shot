from todos.models import TodoList, TodoItem
from django.forms import ModelForm


class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ("name",)


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = (
            "tasks",
            "due_date",
            "is_completed",
            "list",
        )
